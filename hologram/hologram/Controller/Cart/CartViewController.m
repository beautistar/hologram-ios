//
//  CartViewController.m
//  hologram
//
//  Created by JIS on 3/12/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

#import "CartViewController.h"

@interface CartViewController ()

@end

@implementation CartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initView];
}

- (void) initView {
    
    // add background image in navigation bar
    UIImage *img = [UIImage imageNamed:@"banner"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -15, self.view.frame.size.width, 64)];
    [imgView setImage:img];
    // setContent mode aspect fit
    [imgView setContentMode:UIViewContentModeScaleAspectFill];
    
    // add navigation bar title
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 30)];
    [titleLabel setText:@"Cart"];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel setFont:[UIFont systemFontOfSize:28]];
    
    [self.navigationController.navigationBar addSubview:imgView];
    [self.navigationController.navigationBar addSubview:titleLabel];
}



@end
