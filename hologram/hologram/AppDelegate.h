//
//  AppDelegate.h
//  hologram
//
//  Created by JIS on 3/12/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

